
window.Bay = React.createClass({
  render: function(){
    return (
      <tr>
        <td>{this.props.bay.remote_id}</td> <td>{this.props.bay.total_visists}</td>
      </tr>
    );
  }
});

window.BayList = React.createClass({
  render: function(){
    var bay = this.props.bay;
    var bayNodes = this.props.bays.map(function(bay) {
        return(<Bay bay={bay} key={bay.id} />);
      }
    });
    return (
      <div className="bayList">
        {bayNodes}
      </div>
    );
  }
});

window.Bays = React.createClass({
  getInitialState: function() {
    return {bays: window.bayResults};
  },
  lookupBays: function(lookupData){
    var lookupUrl = 'https://'+window.controlshiftDomain+'/efforts/'+window.targetEffort+'/'+'near.json';
    $.ajax({
      url: lookupUrl,
      dataType: 'jsonp',
      data: lookupData
    }).done(function(data) {
      var bays = [data.closest_target].concat(data.other_targets)
      window.bayResults = bays;
      this.setState({bays: bays});
    }.bind(this)).fail(function(xhr, status, err) {
      console.log(lookupUrl, status, err.toString());
    }.bind(this));
  },
  componentDidMount: function() {
      this.lookupBays();
      setInterval(this.lookupBays, this.props.pollInterval);
    },
  handlePlaceChange: function(autocomplete) {
    var geometry = autocomplete.getPlace().geometry;
    if(geometry)
      this.lookupBays({location: {latitude: geometry.location.lat, longitude: geometry.location.lng}});
  },
  render: function() {
    return (
    <div className="bay-lookup">
      <LocationLookupForm effortLookupRef="effort-lookup" onPlaceChange={this.handlePlaceChange} />
      <BayList bays={this.state.bays} />
    </div>
    );
  }
});






