window.BayVisits = React.createClass({
  render: function(){
    if(this.props.bays.visits.length < 1){
      return(<h3>There have been  no visits<h3/>);
    }else{
      return(
        table
        <BayVisit bay={bay} key={bay.slug} />
      );
    }
  }
});

window.BayVisit= React.createClass({
  render: function(){
    return(
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    )
  }
});

window.BayPage = React.createClass({
  getInitialState: function() {
    return {bay: {}};
  },
  componentDidMount: function(){
    var lookupUrl = window.railsRoot+'/bays/'+this.props.params.slug+'.json';
    $.ajax({
      url: lookupUrl,
      dataType: 'jsonp'
    }).done(function(data) {
      this.setState({bay: data});
    }.bind(this)).fail(function(xhr, status, err) {
      console.error(lookupUrl, status, err.toString());
    }.bind(this));
  },
  render: function(){
    return(
      <div>
        <small><Link to="/">&laquo; Back</Link></small>
        <h1>Bay ID #{bay.remote_id}</h1>
        <h2>Visits</h2>
        <BayVists bay={this.state.bay.visists} />
      </div>
      );
  }
});
